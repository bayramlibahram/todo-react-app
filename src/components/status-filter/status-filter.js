import React, { Component } from "react"

class StatusFilter extends Component {
    buttons = [
        {name: 'all', label: 'All'},
        {name: 'active', label: 'Active'},
        {name: 'done', label: 'Done'},
    ]

    render() {

        const {filter, onFilterChange} = this.props;

        const buttons = this.buttons.map(({name, label}) => {
            const isActive = name === filter
            const btnClass = isActive ? 'btn-secondary' : 'btn-outline-secondary'
            return <button
                key={name}
                className={`btn ${btnClass}`}
                onClick={() => onFilterChange(name)}>
                {label}
            </button>
        })


        return (
            <div className="btn-group">
                {buttons}
            </div>
        )
    }
}

export default StatusFilter