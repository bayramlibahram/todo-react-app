import React from 'react';
import './todo-list-item.css'

const TodoListItem = (props) => {

    const {
        label,
        done,
        important,
        onItemDelete,
        onItemImportant,
        onItemDone
    } = props

    let itemClass = 'todo-list-item shadow-sm '

    if (important) itemClass += 'important ';

    if (done) itemClass += 'done ';

    return (
        <span className={itemClass}>
                <span className='todo-label' onClick={onItemDone}>
                    {label}
                </span>
                <span>
                     <button onClick={onItemImportant} className="btn btn-warning me-2">
                         <i className="fas fa-exclamation"></i>
                    </button>
                     <button className="btn btn-danger" onClick={onItemDelete}>
                         <i className="far fa-trash-alt"></i>
                    </button>
                </span>
            </span>
    )
}

export default TodoListItem