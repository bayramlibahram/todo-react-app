import React, {Component} from 'react'

import './item-add-from.css'

class ItemAddFrom extends Component {

    state  = {
        label:''
    }

    onInputChange = (e) =>{
       this.setState({
           label: e.target.value,
       })
    }

    onSubmit = (e) =>{
        e.preventDefault();
        this.props.onItemAdded(this.state.label)
        this.setState({
            label: ''
        })
    }

    render() {

        return (
            <form className="item-add-form" onSubmit={this.onSubmit}>
                <input
                    type="text"
                    className="form-control"
                    placeholder="type todo title"
                    onChange={this.onInputChange}
                    value={this.state.label}
                />
                <button className="btn btn-primary">Add</button>
            </form>
        )
    }

}

export default ItemAddFrom