import React, {Component}from "react";

// const SearchPanel = ({onItemSearch}) => {
//
//     const onItemFind = (e) =>{
//         onItemSearch(e.target.value)
//     }
//
//     return (
//         <input
//             type="text"
//             className="form-control"
//             placeholder="Type the search todo file"
//             onChange={onItemFind}
//         />
//     )
// }


class SearchPanel extends Component {

    state = {
        label:''
    }

    onItemFind = (e) => {
        this.setState(({label}) => {
            return {
                label: e.target.value,
            }
        });
        this.props.onItemSearch(e.target.value);
    }

    render() {
        return (
            <input
                type="text"
                className="form-control"
                placeholder="Type the search todo file"
                onChange={this.onItemFind}
                value = {this.state.label}
            />
        )
    }

}

export default SearchPanel