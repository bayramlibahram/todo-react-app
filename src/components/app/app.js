import React, {Component} from 'react'
import AppHeader from "../app-header"
import SearchPanel from "../search-panel";
import StatusFilter from "../status-filter";
import TodoList from "../todo-list";
import ItemAddFrom from "../item-add-form";
import './app.css'


class App extends Component {

    maxId = 100

    state = {
        todos: [
            {
                label: 'Learn Javascript',
                important: false,
                done: false,
                id: 1
            },
            {
                label: 'Learn NodeJS',
                important: false,
                done: false,
                id: 2
            },
            {
                label: 'Learn React js',
                important: false,
                done: false,
                id: 3
            },
            {
                label: 'Learn React Native',
                important: false,
                done: false,
                id: 4
            }
        ],
        searchText:'',
        filter: 'all'
    }

    createTodoItem = (label) => {
        return {
            label: label,
            important: false,
            done: false,
            id: this.maxId++
        }
    }

    onItemAdded = (itemValue) => {
        const newItem = this.createTodoItem(itemValue)
        this.setState(({todos}) => {

            const newArr = [...todos, newItem]
            return {
                todos: newArr,
            }
        })
    }

    onItemDelete = (id) => {
        this.setState(({todos}) => {
            const newArr = todos.filter(todo => todo.id !== id)
            return {
                todos: newArr
            }
        })
    }

    toggleProperty = (arr, id, propertyName) => {
        const idx = arr.findIndex(todo => todo.id === id);
        const oldTodo = arr[idx]
        const newItem = {...oldTodo, [propertyName]: !oldTodo[propertyName]}
        const newArr = arr.map(todo => todo.id === id ? newItem : todo)
        return newArr
    }

    onItemImportant = (id) => {
        this.setState(({todos}) => {
            return {
                todos: this.toggleProperty(todos, id, 'important')
            }
        })
    }

    onItemDone = (id) => {
        this.setState(({todos}) => {
            return {
                todos: this.toggleProperty(todos, id, 'done')
            }
        })
    }

    onItemSearch = (value) => {
        this.setState(({searchText}) =>{
            return {
                searchText:value
            }
        })

    }

    search = (items, value = '') => {
        if(value.length === 0) {
            return items
        }
        const todoItems = items.filter(item => item.label.toLowerCase().indexOf(value.toLowerCase()) > -1)

        return  todoItems
    }

    onFilterChange = (value) =>{
       this.setState(({filter}) => {
           return {
               filter:value
           }
       })
    }

    filter(items, filter){
        switch (filter) {
            case 'all' : return items
            case 'active' :  return items.filter(item => !item.done)
            case  'done' : return  items.filter(item => item.done)
        }
    }

    render() {

        const {todos, searchText, filter} = this.state;
        const doneCount = todos.filter(todo => todo.done).length;
        const todoCount = todos.length - doneCount;
        const todoItems = this.filter(this.search(todos, searchText), filter )
        return (
            <div className='todo-app shadow'>
                <AppHeader todo={todoCount} done={doneCount}/>
                <div className="top-panel">
                    <SearchPanel onItemSearch={this.onItemSearch}/>
                    <StatusFilter filter={filter} onFilterChange = {this.onFilterChange}/>
                </div>
                <TodoList
                    todos={todoItems}
                    onItemDelete={this.onItemDelete}
                    onItemImportant={this.onItemImportant}
                    onItemDone={this.onItemDone}
                />
                <ItemAddFrom onItemAdded={this.onItemAdded}/>
            </div>
        )
    }

}

export default App
