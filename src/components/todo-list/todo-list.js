import React from 'react';
import './todo-list.css'
import TodoListItem from "../todo-list-item";

const TodoList = ({todos, onItemDelete, onItemImportant, onItemDone}) => {
    const todoList = todos.map(({id, ...otherFields}) => {
        return (
            <li key={id} className="list-group-item">
                <TodoListItem
                    onItemImportant = { () => onItemImportant(id) }
                    onItemDelete = { () => onItemDelete(id) }
                    onItemDone = {() => onItemDone(id)}
                    {...otherFields}
                />
            </li>
        )
    });
    return (
        <ul className="list-group todo-list">
            {todoList}
        </ul>
    )
}

export default TodoList;