import React from "react";
import './app-header.css'

const AppHeader = ({todo, done}) => {
    return (
        <div className="app-header">
            <h1 className="text-primary text-uppercase">My todo list</h1>
            <p className="p-0 m-0 text-secondary">
                {todo} more todo and {done} done
            </p>
        </div>
    )
}

export default AppHeader